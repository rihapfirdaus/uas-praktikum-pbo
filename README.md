# UAS-Praktikum PBO
Proyek Spotify Clone menggunakan Java dengan Gradle non-modular; GUI menggunakan JavaFX, FXML dan CSS; Webservice menggunakan SpringBoot dan Jackson; Database menggunakan Spring Jdbc dan Server MySQL yang bisa diakses melalui db4free.net; Proyek ini juga menyimpan Login Session menggunakan Spring Session dengan Regis.

Sampai saat ini Poyek sudah bisa melakukan validasi login dan signup dengan pendekatan webservice, mendeteksi file musik yang ada di database, memutarnya dan berpindah antar lagu, serta mengatur volume dari lagu. Proyek ini akan terus berkembang. 

[repository gitlab proyek spotifai](https://gitlab.com/rihapfirdaus/spotifai-with-gui-and-database-connection/-/tree/main)

# No 1
Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 
- Use case user
- Use case manajemen perusahaan
- Use case direksi perusahaan (dashboard, monitoring, analisis)

---
1. Use case User
	- bisa membuat akun
	- bisa menggunakan username yang belum digunakan oleh user lain
	- bisa bebas memilih nama akun
	- bisa bebas menambahkan informasi-informasi akun lainnya, seperti foto profil dan tanggal lahir
	- bisa mengakses aplikasi menggunakan akun
	- bisa mengubah password ketika lupa atau ketika diinginkan
	- bisa mengubah semua informasi-informasi pada akun
	- bisa melihat lagu-lagu yang tersedia
	- bisa menambahkan lagu menjadi favorit
	- bisa membuat playlist dan menambahkan lagu kedalamnya
	- bisa melihat lagu yang difavoritkan
	- bisa melihat playlist lagu yang telah dibuatnya atau dibuat orang lain
	- bisa mencari lagu sesuai yang diinginkan berdasarkan judul, artist, ataupun potongan lirik
	- bisa memutar lagu yang diinginkan
	- bisa berpindah antar lagu (sebelum/sesudah)
	- bisa melihat lirik lagu yang sedang diputar apabila tersedia
	- bisa melihat informasi lagu seperti nama artis, album
	- bisa menghentikan pemutaran lagu
	- bisa melihat podcast-podcast yang tersedia
	- bisa mencari podcast yang diinginkan
	- bisa memutar podcast sesuai yang diinginkan
	- bisa berpindah antar podcast
	- bisa menghentikan pemutaran podcast
	- bisa melihat konten yang sedang populer
	- bisa melihat riwayat konten yang telah diputar
	- bisa membagikan konten
	- bisa memindai konten menggunakan barcode khas Spotify
	- bisa memperbesar dan memperkecil volume
	- bisa beralih ke akun premium
2. Use case manajemen perusahaan
	- bisa melihat data banyaknya user yang premium dan tidak premium
	- bisa melihat pendapatan per periode tertentu
	- bisa mengupdate performa aplikasi
	- bisa menambah atau mengupgrade fitur-fitur aplikasi
	- bisa melihat data konten yang banyak diminati user
3. Use case direksi perusahaan
	- bisa melihat konten yang sedang hits dan paling sering diputar oleh user
	- bisa menambahkan konten baru
	- bisa mengelola konten
	- bisa mengubah konten
	- bisa mengatur playlist
	- bisa menyediakan pemutaran lagu berdasarkan preferensi user

# No 2
Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

---


# No 3
Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

---
Pada proyek ini saya menggunakan Single Responsibility Principle(SRP) yang mana prinsip ini menyatakan bahwa sebuah kelas atau modul harus memiliki satu dan hanya satu tanggung jawab tunggal. Pada implementasinya saya memisahkan fungsi-fungsi yang berbeda ke dalam kelas-kelas terpisah, sehingga setiap kelas fokus pada tugas yang spesifik. Penggunaannya dapat dilihat pada Class LoginController, SignupController, dan class-class lainnya dibawah ini.

- class LoginController, digunakan sebagai controller untuk halaman login (seperti dalam design pattern mvc class ini bertanggung jawab sebagai perantara antara model dan view) untuk mengambil data dari form yang terdapat pada halaman FXML Login, mengarahkan ke halaman dashboard setelah melakukan validasi dengan data-data login
![solid](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/Solid-LoginController.png)

- class SignupContoller, digunakan sebagai controller untuk halaman signup (seperti dalam design pattern mvc class ini bertanggung jawab sebagai perantara antara model dan view) untuk mengambil data dari form yang terdapat pada halaman FXML Login, mengarahkan ke halaman dashboard setelah melakukan validasi dengan data-data login, disana ada object getUser dari direktori model yang disini fungsinya mengecek apakah username yang digunakan sudah dipakai atau belum karena username disini bersifat unique. lalu ada setUser dengan parameter bertipe data User yang akan memanggil endpoint dan menambahkan data baru ke database mysql.
![solid](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/Solid-SignupController.png)

- class InputNull, untuk memvalidasi bahwa form tersebut tidak kosong
![solid](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/Solid-InputNull.png)

- class InputPassword, untuk memvalidasi input password dan repassword pada form signup telah sama
![solid](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/Solid-InputPassword.png)

- class PasswordAuthentication, untuk memvalidasi data login sebelum diarahkan ke halaman utama
![solid](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/Solid-PasswordAuthentication.png)

# No 4
Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

----
Proyek ini menggunakan Design Pattern MVC (model-view-controller) dimana setiap program dipisah ke dalam tiga direktori berbeda sesuai dengan fungsionalitasnya. Pada direktori model hanya boleh terdapat program yang berkaitan dengan data dan logika bisnis aplikasi. Pada direktori view hanya boleh terdapat program untuk mengatur tampilan dan antarmuka pengguna. dan pada direktori controller hanya boleh terdapat program yang  bertindak sebagai perantara antara model dan view, seperti menerima input dari pengguna melalui view. 

Keuntungan dari menggunakan design pattern mvc ini adalah pemisahan yang jelas antara logika bisnis, tampilan dan interaksi pengguna sehingga memungkinkan pengembangan aplikasi menjadi lebih terstukrtur dan mudah dipelihara. 

penggunaan design pattern ini dapat dilihat dari  pemisahan direktori antara model, view dan controller yang masing-masing berisi program yang sesuai dengan fungsionalitasnya. 

![mvc](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/MVC-path1.png)
![mvc](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/MVC-path2.png)
![mvc](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/MVC-path3.png)

# No 5
Mampu menunjukkan dan menjelaskan konektivitas ke database

----
Proyek ini menggunakan database mySQL dengan server db4free.net yang dapat diakses dari mana saja. Konektivitas ke database ini mengimplementasikan webservice menggunakan Spring Jbdc. 

![dbconnection](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/DBConnection.png)

Untuk mendapatkan data yang diinginkan dari database langkah-langkahnya sama dengan nomor 6 (menggunakan web service)

# No 6
Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

---
Proyek ini mengimplementasikan webservice menggunakan framework Spring JDBC untuk menghubungkan aplikasi ke database MySQL melalui RESTful API. Berikut adalah contoh penggunaan webservice pada proyek ini untuk mengambil data pengguna (users) dari database. 
1. Karena saya menggunakan gradle maka perlu menambahkan dependency terlebih dahulu ke file gradle.build seperti dibawah ini:

```java
dependencies {
    //dependensi ikonli(untuk ikon)
	implementation 'org.kordamp.ikonli:ikonli-javafx:12.3.1'
	implementation 'org.kordamp.ikonli:ikonli-fontawesome5-pack:12.3.1'
    //dependensi untuk spring jdbc
	implementation 'org.springframework.boot:spring-boot-starter-data-jdbc'
	implementation 'org.springframework.boot:spring-boot-starter-web'
    //dependensi untuk jackson (deserialisasi json)
	implementation 'com.fasterxml.jackson.core:jackson-core:2.14.2'
	implementation 'com.fasterxml.jackson.core:jackson-databind:2.15.1'
    //dependensi untuk java fx
	implementation 'org.openjfx:javafx-controls:17.0.1:win'
	implementation 'org.openjfx:javafx-media:17.0.1:win'
    //dependensi untuk mysql connector
	runtimeOnly 'com.mysql:mysql-connector-j'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

ini semua dependensi yang saya butuhkan pada proyek ini. 

2. Buat class Data Access Object (DAO) untuk mengkonversi data dari objek yang diambil dari database.

![crud](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/9e083f20773e357182f66c9255feea2358e0e530/assets/webservice-crud-user-1.png)

3. Buat class interface yang mewakili entitas data yang akan digunakan dalam webservice. Interface ini akan mendefinisikan metode-metode untuk mengakses data dari database.

![crud](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/9e083f20773e357182f66c9255feea2358e0e530/assets/webservice-crud-user-2.png)

4. Implementasikan setiap metode dari class interface webservice. Setiap metode ini akan mengambil data dari database menggunakan class DAO yang telah dibuat sebelumnya.

![crud](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/9e083f20773e357182f66c9255feea2358e0e530/assets/webservice-crud-user-3.png)

5. Tentukan endpoint untuk webservice dan response dari setiap endpoint.

![crud](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/9e083f20773e357182f66c9255feea2358e0e530/assets/webservice-crud-user-4.png)

6. Akses endpoint webservice yang telah ditentukan untuk mendapatkan data yang diinginkan dari aplikasi. Data tersebut akan diterima dalam format yang telah ditentukan dan siap digunakan dalam aplikasi. Berikut contoh penggunaan dalam mengambil data user dan menambahkan data user ke database menggunakan webservice. 

![crud](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/9e083f20773e357182f66c9255feea2358e0e530/assets/webservice-crud-user-5.png)

![crud](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/webservice-crud-user-6.png)


# No 7
Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

---
framework yang digunakan untuk GUI pada proyek ini yaitu JavaFX dengan controllernya Java dan menggunakan beberapa icon yang didapatkan dari library Iconli:FontAwesome5, serta untuk membangun antarmukanya digunakan FXML. 
1. halaman login

![login](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/UI-Login.png)
2. halaman signup

![signup](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/UI-Signup.png)
3. dasboard

![dashboard](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/UI-Dashboard.png)

# No 8
Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

---
![implementasi](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/implementasi_gui_webservice.png)
class diatas contoh implementasi untuk mendapatkan data musik dari database dengan mengakses endpoint dan mengambil responsenya, yang kemudian akan dipakai untuk data dalam ui dashboard. 

![implementasi](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/implementasi_gui_webservice_1.png)
class diatas contoh penggunaan method pada class getSong dan mendapatkan data dari database yang kemudian diinisialisasi ke variabel list songs yang akan digunakan dalam ui dashboard.

![implementasi](https://gitlab.com/rihapfirdaus/uas-praktikum-pbo/-/raw/main/assets/UI-Dashboard.png)
ui dashboard setelah berhasil mengambil data songs dari endpoint, dan menampilkannya


# No 9
Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

---
[demo aplikasi](https://youtu.be/YQiwtPHMwjQ)

# No 10
Bonus: Mendemonstrasikan penggunaan Machine Learning pada produk yang digunakan
